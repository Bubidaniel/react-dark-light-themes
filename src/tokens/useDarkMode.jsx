import { useEffect, useState } from 'react'

export const useDarkMode = () => {
  const [theme, setTheme] = useState('light')
  const [componentMethod, setComponentMethod] = useState(false)

  const setMode = mode => {
    window.localStorage.setItem('theme', mode)
    setTheme(mode)
  }

  const handleToggle = () => {
    setMode((theme === 'light' ? 'dark' : 'light'))
  }

  useEffect(() => {
    const localTheme = window.localStorage.getItem('theme')
    localTheme ? setTheme(localTheme) : setMode('light')
    setComponentMethod(true)
  })

  return [theme, handleToggle, componentMethod]
}

export default useDarkMode