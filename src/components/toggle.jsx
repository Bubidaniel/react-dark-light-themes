import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircle } from '@fortawesome/free-solid-svg-icons'

const Toggle = ({
  isLight,
  handleToggle
}) => {

  const Toggle = styled.button`
    cursor: pointer;
    display: flex;
    justify-content: space-between;
    position: relative;
    font-size: 0.5rem;
    margin: 0 auto;
    overflow: hidden;
    padding: 0.5rem;

    background: ${({ theme }) => theme.secondaryColor};
    border: none;
    border-radius: 100px;
    
    width: 5rem;
    height: auto;

    svg {
      height: auto;
      width: 1.5rem;
      transform: ${({ isLight }) => isLight === 'light' ? 'translateX(0)' : 'translateX(2.5rem)'};
      transition: all 0.3s linear;
    }
  `

  return (
    <Toggle isLight={isLight} onClick={handleToggle}>
      <FontAwesomeIcon icon={ faCircle } />
    </Toggle>
  )
}

export default Toggle